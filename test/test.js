let assert = require('assert');

let chai = require('chai');
let chaiHttp = require('chai-http');

chai.use(chaiHttp);
let server = require('../app');
let should = chai.should();


describe('Api test', function () {

    it('Reset counter', (done) => {
        chai.request(server).get('/reset-count')
            .end((err, res) => {
                done();
            });
    });

    it('it should be a number of tracks eql 0', (done) => {
        chai.request(server)
            .get('/count')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.have.property('count').eql(0);
                done();
            });
    });

    it('Send post without count', (done) => {
        chai.request(server)
            .post('/track')
            .type('json')
            .send({'key': 'value'})
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('status').eql('ok');
                done();
            });

    });

    it('it should be a number of tracks eql 0', (done) => {
        chai.request(server)
            .get('/count')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.have.property('count').eql(0);
                done();
            });
    });

    it('Send post with count', (done) => {
        chai.request(server)
            .post('/track')
            .type('json')
            .send({'key': 'value', 'count': true})
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('status').eql('ok');
                done();
            });

    });

    it('it should be a number of tracks eql 1', (done) => {
        chai.request(server)
            .get('/count')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.have.property('count').eql(1);
                done();
            });
    });
});