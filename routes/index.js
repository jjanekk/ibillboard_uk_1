const express = require('express');
const router = express.Router();

const redis = require('redis');
const client = redis.createClient({host: 'redis', port: 6379});

const fs = require('fs');
const counterKeyName = 'count';
const fileName = 'track.txt';


/**
 * Reset counter
 */
router.get('/reset-count', (req, res) => {

    __redisErrorHandler(res);

    client.set(counterKeyName, 0, (err) => {
        console.log(err);
    });
    req.send({message : 'Count was set to 0.'});
});


/**
 * Track
 *
 * If data have count key, inc count.
 */
router.post('/track', (req, res) => {

    __redisErrorHandler(res);

    let data = req.body;

    try {
        __saveDataToFileAdd(fileName, data);
    } catch (e) {
        res.statusCode(500);
        res.send({status: 'error', message: e.message});
    }

    if (data.count) {
        __incRedisCounter(counterKeyName, (err) => {
            __errorResponseHanlder(res, err.message);
        }, () => {
            res.send({status: 'ok'});
        });
    } else {
        res.send({status: 'ok'});
    }

});


/**
 *  Return value of counter.
 */
router.get('/count', (req, res) => {

    __redisErrorHandler(res);

    client.get(counterKeyName, (err, data) => {
        if (err) {
            __errorResponseHanlder(res, err.message);
        } else {
            let redisData = JSON.parse(data);

            if (redisData === null) {
                redisData = 0;
            }

            res.send({count: redisData});
        }
    });
});


function __redisErrorHandler(res) {
    client.on('error', (err) => {
        __errorResponseHanlder(res, err.message);
    });
}


/**
 *
 * @param res
 * @param errorMessage
 * @private
 */
function __errorResponseHanlder(res, errorMessage) {
    res.statusCode(500);
    res.send({status: 'error', message: errorMessage});
}


/**
 * Inc redis counter
 * @private
 */
function __incRedisCounter(counter, errCallback, successCallback) {
    client.get(counter, (err, data) => {
        if (err) return errCallback(err);
        else {
            if (data === null) {
                data = 1;
            } else {
                data++;
            }

            client.set(counter, data, (err) => {
                if (err) return errCallback(err);
                successCallback();
            });
        }
    });
}


/**
 * Add data to file, insert new line after data.
 *
 * @param fileName
 * @param data
 * @private
 */
function __saveDataToFileAdd(fileName, data){
    fs.appendFileSync(fileName, JSON.stringify(data) + '\n', function (err) {
        if (err) throw err;
    });
}

module.exports = router;
