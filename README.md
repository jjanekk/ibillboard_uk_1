#ibillboard DU


Napište v JavaScriptu a v Node.js aplikaci, která přijímá HTTP POST požadavky jen na routě /track
z těla požadavku získá data v JSON formátu
uloží získaná JSON data do souboru na lokálním disku (append)
pokud se v datech vyskytuje parametr count, zvýší o jeho hodnotu položku s klíčem 'count' v databázi Redis
přijímá HTTP GET požadavky jen na routě /count
vrátí hodnotu klíče 'count' v databázi Redis
Napište příslušné unit testy, které zvládnete.